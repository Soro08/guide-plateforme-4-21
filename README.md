# guide-plateforme-4-21

    La plateforme NaN 4.21 sera un resumé de toutes les plateformes développé par NaN.
    Cette plateforme comportera les sections suivantes:

    - Site web NaN CI
    - Plateforme de concours 4.21
    - Plateforme de cours 4.21


## Site web NaN CI

    - Pédagogie
    - Formations

        - Cursus des moins 25ans
        - Tous nos modules

    - Entreprises

        - Recrutez vos Talents
        - Formez vos Talents
        - Développez votre Projet

    - Communautés

        - Espace Cours
        - Espace Concours
        - Résultats & Classements
        - Liste des Certifiés
        - Réseau social
        - Forum

    - News & agenda

## Plateforme de concours 4.21

    Un coucours à N étape sur une durée à défini.

    - Partie 1: `Test Logique`
        
        Ici, il sera question d'une serie de sujet de logique:
        
            - psychotechnique
            - memory game
            - robozzle
            - autre test de logique

    - Partie 2: `Test Quiz` 

        ici, nous pourrions organiser un quiz en ligne par spécialité

        exemple:
            
            La video de spécialisation pour les spécialité back-end

    - Autre test à proposer


## Plateforme de cours 4.21

    Cette plateforme comportera différente partie.
    Notons qu'elle sera par spécialité
    elle sera composé de :

    - Plateforme de cours multiple fonction
        - Cours video
        - Cours PDF
        - Cours descriptif -> https://www.w3schools.com/python/
    
    - Plateforme de Composition
        - Quiz QCM
        - Live coding
    - Plateforme de Battle
        - Un battle par spécialité
            - Live code
            - réalisation de projet


## Plateforme de battle

 A definir


## Autre idée

