# Plateforme cours 4.21

- Parcours
- Modules
- Parties
- Cours


## Modules

    La liste des modules
    Une page qui liste les différents modules:
    - image
    - titre
    - ...

## Partie
    Dans un module, nous avons différentes parties:
    - partie 1 : titre 1
    - partie 2 : titre 1

    Sur cette page, nous avons :
    - la liste des Parties  
        - Titre
        - Date
        - ..
    - un lien vers la page d'exercice
    - un lien vers la page de compisiton

## Cours
    Dans une partie, nous avons plusieurs cours, 
    Un cours peut-être composé:
    - Une video
    - Un fichier pdf
    - Un cours ( transcription )


### Resource

#### Site web 

    - Home page : https://openclassrooms.com/fr/
    - Parcours page : https://openclassrooms.com/fr/paths
    - 
#### Dashbord
    - Lecture cours: Udemy

# Partie Admin

    Cette partie admin est composé de différents sous partie

## Admin Général

    L'espace admin général a les possiblité suivantes:

    - CRUD Modules
    - Crud Parcours
    - Ajouter des module dans les parcours
    - CRUD Enseignants
    - Associer des modules à des Enseignants 
    - Voir la liste des étudiants
        - Etudiant inscrits
        - Etudiant inscrits dans les parcours
    

## Admin professeur

    L'espace admin proffeseur donnera la possiblité aux enseignants de gerer leurs modules et leurs étudiants

    - Ajouter les cours dans les parties des modules
    - Créer des quiz
    - Créer des live code
    - Créer des devoirs
    - Créer des cours live
    - corriger les devoirs des utilisateurs
    - Repondre au questions dans les modules
    - Chat etudiant-professeur

    



