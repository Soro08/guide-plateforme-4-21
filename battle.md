# Battle Plateforme

    Le battle est l'une des plateformes 4.21 qui mettra en avant l'image de NaN CI. Cette plateforme réunira les dévellopeurs de la CI et de L'afrique francofone.
    Il sera question pour nous de nous inspirer de la plateforme ISO GRADE TOSA CODE. L'idée est de faire notre propre TOSA CODE
## Langage
    - JS
    - Python
    - C#
    - C++
    - PHP
    - Rubi
    - Perl
    - ...

    - HTML, CSS
    - SQL

## Parties
    Sur cette plateforme, nous aurons les parties suivantes:
        - Individuelle
        - Groupe
        - Exercices
        - Eliminatoire

### Individuelle

    Un test qui se fera de façon individuel où chaque candidat traitera personnelement ces questions 

    Une compition dans ce sens sera organisé chaque mois (à valider ) par NaN CI en partenariat avec certains entreprise.

    - Question 
        5 à 10 question en fonction du niveau de composition
    - Crictère
        Nombre de question resolut
        La durée
        La taille des fichier
        ...
    - Progression
        Valider une question pour passer à la suivante 

    - Condition de participation
        - Gratuit
        - Payant
    - Recompence
        - En fonction des partenariats

    - Type de competition
        - Nationnale
        - Sous regionnale
        - ...

### La plateforme
    - Dashbord candidat
        L'espace user sera un modèle code signal ou l'user:
        - Verra la liste des exercices qu'il a commencé ou validé
        - Son profile
        - ces badges
        - ...
    - Login-Register (email, gmail, google, facebook)
    - 
    - Menu:
        - Home
        - competition
        - Exercices
        - entreprise
        - contactez-nous
    - Home page 
        Une page de presentation de nan coding suivre les modèles:
        - tosa code : https://www.isograd.com/FR/tosacode.php?target=2
        - code signale: https://codesignal.com/
    - Competition 
        Une liste qui affiche nos différents competitions
        - image
        - Titre 
        - Oragnisateur
        - Date
        - Durée
        - Exercice


        - Detail competitions
            Une page detail competion avec les Exercices
            https://www.isograd.com/FR/solutionconcours.php
    
    - Exercice
        Une page qui affiche les exercices 
        Liste des exercices par niveau modèle https://app.codesignal.com/

        - Detail Exercice ( pareil comme detail competition)
    
    - Entreprise
        Une page qui presente nos offres aux entreprises 
        - https://codesignal.com/products/test/

    - contactez-nous
        Un formulaire de contact









#------------------------------------------------------

## Modèle competion & recrutrement
    Ici nous allons créer les modèles pour le battle (
        - competion
        - recrutrement
        - Banquet d'exercice
    )

    ### Baquet D'exercice
``` 
    - Banquet
        - Titre
        - Type (web, programmation, Base de donnée, multimedia)
        - Description
        - image
    - Exercice_web
        - exo (voir model nancours)
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)
    - Exercice_programmation
        - exo (voir model nancours)
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)
    - Exercice_database
        - exo (voir model nancours)
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)
    - Exercice_multimedia
        - exo (voir model nancours)
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)
``` 
    ### Competition

    - competion
        - competion (voir model competion)
    
    - exo_competition
        - exercice_id (ici, nous choisissons l'exercice en fonction du type de la competion)
    

    ### Recrutement

    - recrutrement
        - recrutrement (voir model recrutrement)

    - exo_recrutement
        - type_exo (les types d'exercice depend des type que l'entreprise choisir pendant la demande)
        - exercice_id (ici, nous choisissons l'exercice en fonction du type de l'exo)

        
    