# Model Battle Exercice

``` 
    class Langage(models.Model):
        lang_id = models.PositiveIntegerField()
        nom = models.CharField(max_length=250)

        status = models.BooleanField(default=True)
        date_add = models.DateTimeField(auto_now_add=True)
        date_upd = models.DateTimeField(auto_now=True)

        class Meta:
            """Meta definition for LangageId."""

            verbose_name = 'Langage'
            verbose_name_plural = 'Langage'

        def __str__(self):
            return '{}:{}'.format(self.lang_id, self.nom)  # TODO


    - Banquet
        - Titre
        - Type (web, programmation, Base de donnée, multimedia)
        - Description
        - image
    


# -------------------- WEB DESIGN


class ExcerciceWebDesign(models.Model):
    TYPES = [
        ('1', 'Html'),
        ('2', 'Css'),
        ('3', 'Javascript')
    ]

    livecoding = models.ForeignKey(LiveCoding, on_delete=models.CASCADE,
                                   related_name='programmation_excercicewebdesign')
    titre = models.CharField(max_length=255)
    exo_type = models.CharField(choices=TYPES, max_length=250)
    ennonce = HTMLField('ennonce')
    exemple = HTMLField('exemple', null=True, blank=True)
    code_html_depart = models.TextField(null=True, blank=True)
    code_css_depart = models.TextField(null=True, blank=True)
    code_js_depart = models.TextField(null=True, blank=True)
    code_html_solution = models.TextField(null=True, blank=True)
    - Champs suplementaire
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)

    date_add = models.DateTimeField(auto_now_add=True)
    date_upd = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Excercice WebDesign'
        verbose_name_plural = 'Excercices WebDesign'

    def __str__(self):
        return str(self.titre)


class ValidationWebDesign(models.Model):
    validation = models.ForeignKey(ExcerciceWebDesign, on_delete=models.CASCADE,
                                   related_name='validation_excercicewebdesign', null=True, blank=True)
    balise = models.CharField(max_length=250)
    contenu_balise = models.TextField(null=True, blank=True)
    use_contenu = models.BooleanField(default=False)
    propriete = models.CharField(null=True, blank=True, max_length=250)
    valeur_propriete = models.TextField(null=True, blank=True)
    use_propriete = models.BooleanField(default=False)

    date_add = models.DateTimeField(auto_now_add=True)
    date_upd = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Validation WebDesign'
        verbose_name_plural = 'Validations WebDesign'

    def __str__(self):
        return str(self.balise)


# -------------------- Programmetion
class ExcerciceProgrammation(models.Model):
    livecoding = models.ForeignKey(LiveCoding, on_delete=models.CASCADE, related_name='programmation_excerciceprogrammation')
    titre = models.CharField(max_length=255)
    ennonce = HTMLField('ennonce')
    editorcode = models.TextField(blank=True, null=True)
    exemple = HTMLField('exemple', blank=True, null=True)

    - Champs suplementaire
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)

    date_add = models.DateTimeField(auto_now_add=True)
    date_upd = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Excercice Programmation'
        verbose_name_plural = 'Excercice Programmation'
 
    def __str__(self):
        return str(self.livecoding)


class ValidationProgrammation(models.Model):
    validation = models.ForeignKey(ExcerciceProgrammation, on_delete=models.CASCADE,
                                   related_name='validation_excerciceprogrammation')
    entree = models.TextField()
    sortie = models.TextField()

    date_add = models.DateTimeField(auto_now_add=True)
    date_upd = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Validation Programmation'
        verbose_name_plural = 'Validation Programmation'

    def __str__(self):
        return str(self.validation)


# -------------------- Database
class ExcerciceDatabase(models.Model):
    livecoding = models.ForeignKey(LiveCoding, on_delete=models.CASCADE, related_name='programmation_excercicedatabase')
    titre = models.CharField(max_length=255)
    TYPES = [
        ('1', 'Query'),
        ('2', 'Mutation')
    ]
    exo_type = models.CharField(choices=TYPES, max_length=250)
    ennonce = HTMLField('ennonce')
    code_sql_create = models.TextField()
    code_solution = models.TextField()
    code_depart = models.TextField(blank=True, null=True)
    code_verification = models.TextField(blank=True, null=True)
    - Champs suplementaire
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)

    date_add = models.DateTimeField(auto_now_add=True)
    date_upd = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Excercice Database'
        verbose_name_plural = 'Excercice Database'

    def __str__(self):
        return str(self.livecoding)




# ---------------------- Multimedia
class ExcerciceMultimedia(models.Model):
    livecoding = models.ForeignKey(LiveCoding, on_delete=models.CASCADE,
                                   related_name='programmation_excercicemultimedia')
    titre = models.CharField(max_length=255)
    ennonce = HTMLField('ennonce')
    ressource = models.FileField(upload_to='images/excercicemultimedia', null=True, blank=True)
    fichier_solution = models.FileField(upload_to='images/excercicemultimedia', null=True, blank=True)
    couleur_ignore = models.CharField(max_length=255, null=True, blank=True)
    - Champs suplementaire
        - niveau (facile, moyen, difficile)
        - duree_moy_reponse
        - exo_competition (True or False: Si True, l'exo ne s'affiche pas dans la partie exercice)
        - AddBy (entreprise)

    date_add = models.DateTimeField(auto_now_add=True)
    date_upd = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Excercice Multimedia'
        verbose_name_plural = 'Excercice Multimedia'

    def __str__(self):
        return str(self.livecoding)

``` 